﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GuitarHeroQTE : MonoBehaviour {
	public class GuitarNote{
		public GuitarNote(GameObject _go, char _c){
			go = _go.GetComponent<Image>();
			c = _c;
		}
		public Image go;
		public char c;
	}

	private PlayerController player;
	public int notesLeft;
	public Image letterSr;
	public Image Box;

	private int _notesLeft;
	public char[] characters;
	public Sprite[] sprites;
	public Vector2 position;
	List<GuitarNote> list;

	bool[] seila;

	public Transform canvas;
	public bool started;

	Dictionary<char, Sprite> dict;

	void Start(){
		seila = new bool[4];

		player = FindObjectOfType<PlayerController> ();
		_notesLeft = notesLeft;
		list = new List<GuitarNote> ();

		dict = new Dictionary<char, Sprite> ();

		for (int i = 0; i < characters.Length; i++) {
			dict [characters [i]] = sprites [i];
		}
	}
	int index = 0;

	bool check = false;

	void Update(){
		if (started && Input.anyKeyDown) {
			GuitarNote g = null;
			foreach(var x in list){
				if (x.go == null) {
					continue;
				}

				if (x.go.rectTransform.position.x >= (Box.rectTransform.position.x - Box.rectTransform.rect.size.x) && x.go.rectTransform.position.x <= (Box.rectTransform.position.x + Box.rectTransform.rect.size.x)) {
					if (x.c == Input.inputString[0]) {
						g = x;
						break;
					}
				}
			}
			if (g != null) {
				print ("entrou");
				list.Remove (g);
				Destroy (g.go);
				if (index == 0) {
					player.transform.DOMoveX (358.3f, (358.3f - player.transform.position.x) / player.velocityStates [player.state]);
					player.anim.SetBool ("isMoving", true);
				}

				if (index == 3) {
					index = 0;
					player.transform.DOKill ();
					player.anim.SetBool ("isMoving", false);
				} else {
					index++;
				}
			} else {
				player.currLife -= player.life / 6;
				player.rb.velocity = Vector2.zero;
			}
		}
	}

	public void StartQTE(){
		player.currLife = player.life;
		Camera.main.GetComponent<CameraController> ().canFollow = true;
		Box.color = Color.white;
		started = true;
		notesLeft = _notesLeft;
		StartCoroutine (spawnLetters ());
	}

	IEnumerator spawnLetters(){
		while (notesLeft > 0) {
			for(int i=0; i<4 && notesLeft != 0; i++){
				int index = Random.Range (0, characters.Length);
				Rigidbody2D go = Instantiate (letterSr, letterSr.rectTransform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
				go.velocity = Vector2.left * 250f;
				go.transform.SetParent (canvas);
				Image img = go.GetComponent<Image> ();
				img.color = Color.white;
				img.sprite = sprites [index];

				GuitarNote k = new GuitarNote (go.gameObject, characters [index]);
				list.Add (k);

				StartCoroutine (destroyAfter(k));
				yield return new WaitForSeconds (.5f);
			}
			yield return new WaitForSeconds (3f);
		}

		yield return null;
		started = false;
	}

	public void end(){
		player.transform.DOKill ();
		notesLeft = 0;
		foreach (var x in list) {
			Destroy (x.go);
		}
		list.Clear ();
		Color color = Box.color;
		color.a = 0;
		Box.color = color;
	}

	public bool Ended(){
		return Mathf.Abs(player.transform.position.x - 358.3f) < .1f ;
	}

	IEnumerator destroyAfter(GuitarNote k){
		yield return new WaitForSeconds (5.5f);
		if (list.Contains(k)) {
			Destroy (k.go.gameObject);
			list.Remove (k);
			player.currLife -= player.life / 6;
			player.rb.velocity = Vector2.zero;
		}
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		Gizmos.DrawSphere ((Vector2)Camera.main.transform.position + position, .4f);
	}
}
