﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PunhetarBotao : MonoBehaviour {
	public SpriteRenderer[] sr;
	public float distance;

	PlayerController player;
	float value;

	float startX;

	public bool check = true;

	void Start(){
		player = FindObjectOfType<PlayerController> ();
	}

	public void StartQTE(){
		
		startX = player.transform.position.x;

		Camera.main.GetComponent<CameraController> ().canFollow = true;
		StartCoroutine (start ());
	}

	IEnumerator start(){
		value = 0f;
		foreach (var x in sr) {
			x.DOFade (1f, 1f);
		}
		while (!Ended() && check) {
			float speed = player.velocityStates [3];

			value -= .001f;

			if(Input.GetKeyDown(KeyCode.Q)){
				value += .1f;
			}

			if (value > 1f) {
				value = 1f;
			} else if (value < 0) {
				value = 0;
			}

			player.rb.velocity = new Vector2 (value * speed, 0);
			player.anim.SetBool ("isMoving", player.rb.velocity.x > 0);

			yield return null;
		}

		check = true;

		foreach (var x in sr) {
			Color color = x.color;
			color.a = 0;
			x.color = color;
		}
	}

	public bool Ended(){
		return player.transform.position.x > startX + distance;
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		Vector2 pos = FindObjectOfType<PlayerController>().transform.position;
		pos.x += distance;
		Gizmos.DrawSphere (pos, .4f);
	}
}
