﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (destroyAfter ());
	}
	
	IEnumerator destroyAfter(){
		yield return new WaitForSeconds (2f);
		Destroy (gameObject);
	}
}
