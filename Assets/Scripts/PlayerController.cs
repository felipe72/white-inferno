﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;


public class PlayerController : MonoBehaviour {
	public enum AudioTypes
	{
		Footsteps, Arrastando, Golpes, Jump, Landing, MontinhoGrupo, MontinhoSingle, PeArrastando, Spawn
	}

	public SpriteRenderer endImage;
	public float[] velocityStates;
	public float crawlDowntime;
	public float crawlUptime;
	public float attackCooldown;
	public float range;
	public Vector3[] behind;
	public Vector3[] front;
	public float jumpForce;
	public int wrapAmount = 15;
	public float attackTimeCinematic;
	public float amountToWin;
	public float amountToLose;
	public SpriteRenderer ButtonA, ButtonD, ButtonF;
	public Vector3 cutscene1Pos;
	public float life;
	public float enemyDamage;
	public SpriteRenderer damageEffect;
	public float restoreTime;
	[HideInInspector]
	public bool canWalk = true;
	[HideInInspector]
	public Vector3 lastCheckpoint;
	[HideInInspector]
	public bool canAttack = true;
	[HideInInspector]
	public bool isWrapped = false;
	[HideInInspector]
	public bool isCutscene = false;
	[HideInInspector]
	public bool isJumping = false;
	[HideInInspector]
	public int state = 0;

	public GameObject precision;
	public Vector2 precisionPos;
	public Vector2[] jumpingPositions;
	public float jumpingTime;
	public ParticleSystem snowDefault, snowHard, snowHardHard;
	[HideInInspector]
	public float currLife;
	[HideInInspector]
	public Rigidbody2D rb;
	public Animator anim;
	private SpriteRenderer sr;
	private bool waitJump = false;
	private bool isStarting = true;
	private bool leftButton = true;
	private int currWrapAmount;
	private GameManager gameManager;
	private List<Enemy> enemies;
	private CameraController _camera;
	private FlashBack flashback;
	[HideInInspector]
	public Image blackOverlay;
	private bool wrapStarted = false;
	private float wrapValue;
	private bool canJump = true;
	private bool cutsceneAttackStarted = false;
	private int jumpingIndex = 0;
	private bool isSnowing = false;
	private AudioController _audio;
	private AudioSource audioSource;

	public AudioClip[] footsteps;
	public AudioClip[] arrastando;
	public AudioClip[] golpes;
	public AudioClip _jump;
	public AudioClip landing;
	public AudioClip montinhoGrupo;
	public AudioClip[] montinhoSingle;
	public AudioClip[] peArrastando;
	public AudioClip spawn;
	public AudioClip telaVermelha;

	public Image credits;
	public Text creditsT;

	void Start(){
		_camera = FindObjectOfType<CameraController> ();
		lastCheckpoint = transform.position;
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		sr = GetComponent<SpriteRenderer> ();
		enemies = new List<Enemy> ();
		gameManager = FindObjectOfType<GameManager> ();
		flashback = FindObjectOfType<FlashBack> ();
		blackOverlay = flashback.blackOverlay;
		_audio = FindObjectOfType<AudioController> ();
		audioSource = GetComponent<AudioSource> ();
		currLife = life;
	}

	void PlayArrastando(){
		Play (arrastando);
	}

	void PlayPeArrastando(){
		Play (peArrastando);
	}

	void Play(AudioClip clip){
		if (footsteps.Contains (clip) || peArrastando.Contains (clip) || arrastando.Contains (clip)) {
			audioSource.volume = .5f;
		} else if(clip == _jump){
			audioSource.volume = .8f;
		}else{
			audioSource.volume = 1f;
		}

		//if (!audioSource.isPlaying) {
			audioSource.clip = clip;
			audioSource.Play ();
		//}
	}

	void Play(AudioClip[] clip){
		
		Play(clip[Random.Range(0, clip.Length)]);
	}

	void PlayFootsteps(){
		Play (footsteps);
	}

	void PlayGolpes(){
		Play (golpes);
	}

	void Update(){
		if (isStarting) {
			return;
		}


		isWrapped = enemies.Count != 0;

		if (isCutscene) {
			canWalk = false;
			canAttack = false;
			canJump = false;
		}


		if (isWrapped) {
			if (anim.GetBool ("release") == false) {
				anim.speed = 0;
			}

			if (leftButton) {
				ButtonD.color = Color.clear;
				ButtonA.color = Color.white;
			} else {
				ButtonA.color = Color.clear;
				ButtonD.color = Color.white;
			}

			if (wrapValue >= .9f) {
				if (!isCutscene) {
					wrapStarted = false;
					isWrapped = false;
					StartCoroutine (release ());
					List<Enemy> _enemies = new List<Enemy> ();
					foreach (var x in enemies) {
						x.ReceiveDamage (10);
						_enemies.Add (x);
					}

					foreach (var x in _enemies) {
						enemies.Remove (x);
					}
				} else {
					if (enemies.Count > 0) {
						if (!cutsceneAttackStarted) {
							cutsceneAttackStarted = true;
							StartCoroutine (attackCinematic ());
						}
					}
				}
			} else if ((Input.GetKeyDown (KeyCode.A) && leftButton) || (Input.GetKeyDown (KeyCode.D) && !leftButton)) {
				leftButton = !leftButton;
				wrapValue += amountToWin;
			}
		} else {
			ButtonA.color = Color.clear;
			ButtonD.color = Color.clear;
		}

		for (int i = 1; i <= 5; i++) {
			if (state == i - 1) {
				anim.SetBool ("player" + i.ToString (), true);
			} else {
				anim.SetBool ("player" + i.ToString (), false);
			}
		}

		float horizontal = Input.GetAxis ("Horizontal");
		if (canWalk) {
			if (horizontal != 0) {
				//Play (footsteps);
			}
			Vector2 tmp = rb.velocity;
			tmp.x = horizontal * velocityStates [state] / (isSnowing ? 2 : 1);
			rb.velocity = tmp;
		} else if(!isCutscene){
			Vector2 tmp = rb.velocity;
			tmp.x = 0;
			rb.velocity = tmp;
		}

		if (!isCutscene) {
			sr.flipX = rb.velocity.x == 0 ? sr.flipX : rb.velocity.x < 0;
		}

		if (!isCutscene) {
			anim.SetBool ("isMoving", rb.velocity.x != 0);
		}

		if (Input.GetKeyDown (KeyCode.F) && canAttack && state == 0) {
			canAttack = false;
			canWalk = false;

			int index = Random.Range (0, 2);
			anim.SetTrigger ("attack" + index.ToString ());
			StartCoroutine (cooldown ());
		}

		if (Input.GetKeyDown (KeyCode.Space) && !isJumping && canJump && state <= 2) {
			Play (_jump);
			anim.SetTrigger ("jump");
			Vector2 tmp = rb.velocity;
			tmp.y = jumpForce;
			rb.velocity = tmp;
			isJumping = true;
			waitJump = true;
			StartCoroutine (jump ());
		}
	}

	public void EndCutscene(){
		isCutscene = false;
		canWalk = true;
		snowHard.gameObject.SetActive (false);
		if(!gameEnding)
			snowHardHard.gameObject.SetActive (false);
		canAttack = true;
		if (state < 3) {
			canJump = true;
		}
	}

	IEnumerator release(){
		anim.speed = 1;
		anim.SetBool ("release", true);
		Play (montinhoGrupo);
		yield return new WaitForSeconds (.5f);
		anim.SetBool ("release", false);
		while (enemies.Count != 0) {
			yield return null;
		}
		canWalk = true;
		canAttack = true;
		canJump = true;
		if (!isCutscene) {
			StartCoroutine (restore ());
		}
	}

	IEnumerator restore(){
		float remainingTime = restoreTime;
		bool check = true;

		Tweener tween = null;
		tween = damageEffect.DOFade (0f, restoreTime).OnUpdate (() => {
			currLife = (1 - damageEffect.color.a) * life;

			if (enemies.Count > 0) {
				tween.Kill();
			}
		});
		yield return tween.WaitForCompletion();
	}

	IEnumerator attackCinematic(){
		float remainingTime = attackTimeCinematic;
		bool check = false;
		while (remainingTime >= 0) {
			ButtonA.color = Color.clear;
			ButtonD.color = Color.clear;
			ButtonF.color = Color.white;
			if (Input.GetKeyDown (KeyCode.F)) {
				check = true;
				break;
			}

			remainingTime -= Time.deltaTime;
			yield return null;
		}

		ButtonF.color = Color.clear;

		if (check) {
			Play (montinhoSingle);
			anim.speed = 1f;
			anim.SetBool ("release", true);
			yield return new WaitForSeconds (.1f);
			enemies [0].ReceiveDamage (10);
			enemies.Remove (enemies [0]);
			if (enemies.Count == 0) {
				ButtonA.color = Color.clear;
				ButtonD.color = Color.clear;
				ButtonF.color = Color.clear;
				isWrapped = false;
				flashback.Next ();
			}
			yield return new WaitForSeconds (.4f);
			anim.SetBool ("release", false);
			anim.speed = 0f;
		} else {
			currLife -= life / 3;
			Color color = damageEffect.color;
			color.a = 1 - currLife / life;
			damageEffect.color = color;
			// Perde vida aqui
		}

		cutsceneAttackStarted = false;
		wrapValue = 0f;
		//currWrapAmount = wrapAmount;

	}

	public void Wrap(Enemy enemy){
		if (!wrapStarted) {
			wrapStarted = true;
			StartCoroutine (wrapUpdate ());
		}

		canWalk = false;
		canAttack = false;
		isWrapped = true;
		canJump = false;

		enemies.Add (enemy);

		if (currWrapAmount <= 0) {
			currWrapAmount = wrapAmount;
		}
	}

	IEnumerator wrapUpdate(){
		wrapValue = .5f;

		while (wrapStarted) {
			wrapValue -= amountToLose * enemies.Count;
			if (!isCutscene) {
				currLife -= enemyDamage * enemies.Count;
			}

			if (currLife <= 0) {
				Color _color = damageEffect.color;
				_color.a = 0;
				damageEffect.color = _color;
				gameManager.Restart (lastCheckpoint.x);
				currLife = life;
				enemies.RemoveRange (0, enemies.Count);
				anim.speed = 1f;
				canJump = true;
				EndCutscene ();
				canAttack = true;
				wrapStarted = false;
				break;
			}

			Color color = damageEffect.color;
			color.a = 1 - currLife / life;
			damageEffect.color = color;

			if (wrapValue < 0) {
				wrapValue = 0;
			}
			yield return new WaitForEndOfFrame();
		}
	}

	public void setMove(){
		if (enemies.Count == 0) {
			canWalk = true;
		}
	}

	public void StartGame(){
		isStarting = false;
	}

	IEnumerator jump(){
		anim.ResetTrigger ("land");
		yield return new WaitForSeconds (.4f);
		waitJump = false;

	}

	IEnumerator cooldown(){
		float remainingTime = attackCooldown;
		while (remainingTime >= 0) {
			remainingTime -= Time.deltaTime;

			float num = sr.flipX ? -1 : 1;

			RaycastHit2D hit = Physics2D.Raycast (transform.position, num * Vector2.right, range, 1 << LayerMask.NameToLayer ("Enemies"));
			if (hit) {
				hit.collider.GetComponent<Enemy> ().ReceiveDamage (1);
			}
			yield return new WaitForEndOfFrame ();
		}

		yield return new WaitForSeconds(attackCooldown);

		while (!canWalk && !isCutscene && enemies.Count != 0) {
			yield return null;
		}

		canAttack = true;
	}

	bool gameEnding;
	Coroutine co;


	public void ChangeState(){
		_audio.PlayNext ();
		StartCoroutine (changeState ());
		lastCheckpoint = transform.position;

		state = (state + 1);
		if (state == velocityStates.Length - 2)
			co = StartCoroutine (doCrawlCooldown ());
		if (state > 4) {
			gameEnding = true;
			state = 4;
		}
	

		//anim.SetTrigger ("change");
	}

	public void Cutscene5(){
		snowHardHard.gameObject.SetActive (true);
		if (co != null) {
			StopCoroutine (co);
		}
		canWalk = false;
		canJump = false;
		canAttack = false;
		flashback.Next ();
	}

	private IEnumerator changeState(){
		anim.speed = 1f;
		Vector3 originalPos = Camera.main.transform.position;
		int originalOrder = sr.sortingOrder;
		SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer> ();
		bool check = true;
		foreach (var x in srs) {
			x.sortingOrder = 1000 - (check ? 0 : 1);
			check = false;
		}

		float time = .5f;
		Vector3 tmpp = transform.position;
		tmpp.z = Camera.main.transform.position.z;
		Camera.main.transform.DOMove (tmpp, time);
		float originalHeight = Camera.main.orthographicSize * 2f;
		yield return Camera.main.DOOrthoSize (5, time).OnUpdate(() => {
			Vector3 pos = originalPos;
			pos.y -= .5f * (originalHeight - Camera.main.orthographicSize * 2f);
			Camera.main.transform.position = pos;
		}).SetEase(Ease.InOutCubic).WaitForCompletion();
		blackOverlay.color = Color.red;
		sr.color = Color.black;
		anim.SetBool ("takeHit", true);
		Play (telaVermelha);
		Camera.main.DOShakePosition (.5f, .5f);
		yield return new WaitForSeconds (.2f);
		yield return new WaitForSeconds (time);
		sr.color = Color.white;
		blackOverlay.color = Color.clear;
		check = true;
		foreach (var x in srs) {
			x.sortingOrder = check ? 1 : 0;
			check = false;
		}
		Camera.main.transform.DOMove (originalPos, time/2f);
		yield return Camera.main.DOOrthoSize (6, time/2f).WaitForCompletion ();
		yield return new WaitForSeconds (.2f);
		anim.SetBool ("takeHit", false);
		_camera.canFollow = true;

		if (!gameEnding) {
			print ("oi");
			EndCutscene ();
		}

		WaveStarter.index++;
		if (state == 2) {
			StartCoroutine (snowing());
		}
		if (gameEnding) {
			Vector3 pos = transform.position;
			pos.y = 0;
			_camera.LockCamera (new Vector3 (7, 0, -10) + pos, 3f, 6);
		}
	}

	IEnumerator snowing(){
		while (state == 2 && !isCutscene) {
			isSnowing = false;
			snowHard.gameObject.SetActive (false);
			yield return new WaitForSeconds (Random.Range (2f, 4f));
			if (isCutscene) {
				break;
			}
			isSnowing = true;
			snowHard.gameObject.SetActive (true);
			yield return new WaitForSeconds (Random.Range (6f, 8f));
		}
	}

	public void crawl(){
		co = StartCoroutine (doCrawlCooldown ());
	}

	public IEnumerator doCrawlCooldown(){
		while (!ended && !check2) {
			if (isCutscene) {
				yield return null;
			}
			canWalk = true;
			yield return new WaitForSeconds (crawlUptime);
			if (check2) {
				break;
			}
			canWalk = false;
			yield return new WaitForSeconds (crawlDowntime);
		}
	}

	bool ended;

	bool check2 = false;

	void seila(){
		anim.SetBool ("once", true);
		canWalk = false;
		check2 = true;
		canWalk = false;
		StartCoroutine (seila2 ());
	}

	IEnumerator seila2(){
		yield return new WaitForSeconds (2f);
		endImage.DOFade (1f, 4f);

		yield return new WaitForSeconds (8f);

		yield return credits.DOFade (1f, 2f).WaitForCompletion ();
		yield return creditsT.DOFade (1f, 2f).WaitForCompletion ();

		yield return new WaitForSeconds (5f);

		SceneManager.LoadScene (0);
	}

	IEnumerator seila3(){
		yield return new WaitForSeconds (2f);

		sr.sortingOrder = 1000;

		anim.SetBool ("end",true);
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.name == "End") {
			canWalk = false;
			ended = true;
			StartCoroutine (seila3 ());
		}

		WaveStarter wave = other.GetComponent<WaveStarter> ();
		if (wave) {
			if (wave.Trigger ()) {
				canWalk = false;
			}
		}

		QuicktTimeEvent qte = other.GetComponent<QuicktTimeEvent> ();
		if (qte) {
			qte.StartQTE ();
		}

		if(other.gameObject.layer == LayerMask.NameToLayer("KillPlayer")){
			gameManager.Restart (lastCheckpoint.x);
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.collider.gameObject.layer == LayerMask.NameToLayer ("Floor") && !waitJump && isJumping) {
			isJumping = false;
			anim.SetTrigger ("land");
			Play (landing);
		}
	}

	public void Restart(){
		Vector3 tmp = Camera.main.transform.position;
		tmp.x = lastCheckpoint.x;
		Camera.main.transform.position = tmp;
		currLife = life;
		Color _color = damageEffect.color;
		_color.a = 0;
		damageEffect.color = _color;

		transform.position = lastCheckpoint;
		canWalk = true;
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		foreach (var x in behind) {
			Gizmos.DrawSphere (transform.position + x, .4f);
		}
		Gizmos.color = Color.blue;
		foreach (var x in front) {
			Gizmos.DrawSphere (transform.position + x, .4f);
		}

		Gizmos.color = Color.black;
		foreach (var x in jumpingPositions) {
			Gizmos.DrawSphere (x, .4f);
		}

		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere (cutscene1Pos, .4f);
		Gizmos.DrawSphere ((Vector2)transform.position + precisionPos, .4f);
	}

	public void Cutscene2(){
		StartCoroutine (cutscene2 ());
	}

	IEnumerator cutscene2(){
		isCutscene = true;
		PrecisionQTE qte = Instantiate (precision, precisionPos +(Vector2) transform.position, Quaternion.identity).GetComponent<PrecisionQTE>();
		qte.transform.SetParent (Camera.main.transform);
		while (jumpingIndex < 3) {
			if(Input.GetKeyDown(KeyCode.F)){
				Play (_jump);
				if (qte.GetResult ()) {
					qte.ScaleDown ();
					anim.SetTrigger ("jump");

					float vx = -(transform.position.x - jumpingPositions[jumpingIndex++].x) / jumpingTime;
					float vy = -Physics2D.gravity.y * rb.gravityScale * jumpingTime / 2f;

					rb.velocity = new Vector2 (vx, vy);
					yield return new WaitForSeconds (jumpingTime);
					rb.velocity = Vector2.zero;
					anim.SetTrigger ("land");
					Play (landing);

					Vector3 pos = Camera.main.transform.position;
					pos.x = transform.position.x;

					if (jumpingIndex != 3) {
						_camera.LockCamera (new Vector3 (7, 0, -10) + pos, 2f, 1000);
					}
					//_camera.canFollow = true;
				} else {
					anim.SetTrigger ("jump");
					float vx = -(transform.position.x - jumpingPositions[jumpingIndex++].x + 4f) / jumpingTime;
					float vy = -Physics2D.gravity.y * rb.gravityScale * jumpingTime / 2f;

					rb.velocity = new Vector2 (vx, vy);
					yield return new WaitForSeconds (jumpingTime);
					rb.velocity = Vector2.zero;

					Vector3 pos = Camera.main.transform.position;
					pos.x = transform.position.x;

					_camera.LockCamera (new Vector3 (7, 0, -10) + pos, 2f, 10000);
					isJumping = true;
					canJump = true;
					canWalk = true;
					canAttack = true;
					isCutscene = false;
					Destroy (qte.gameObject);
					jumpingIndex = 0;
					break;
				}
			}
			yield return null;
		}
		Destroy (qte.gameObject);
		if (jumpingIndex == 3) {
			flashback.Next ();
		}

	}

	public void Cutscene4(){
		StartCoroutine (cutscene4 ());
	}

	IEnumerator cutscene4(){
		GuitarHeroQTE qte = FindObjectOfType<GuitarHeroQTE> ();
		qte.StartQTE ();
		print ("oi");
		while (!qte.Ended ()) {
			print ("ola");
			Color color = damageEffect.color;
			color.a = 1 - currLife / life;
			damageEffect.color = color;

			if (currLife <= 0) {
				Color _color = damageEffect.color;
				_color.a = 0;
				damageEffect.color = _color;
				gameManager.Restart (lastCheckpoint.x);
				enemies.RemoveRange (0, enemies.Count);
				anim.speed = 1f;
				canJump = true;
				EndCutscene ();
				canAttack = true;
				wrapStarted = false;
				qte.end ();
				break;
			}

			yield return null;
		}

		if (currLife > 0) {
			StartCoroutine (restore ());
			flashback.Next ();
			anim.SetBool ("isMoving", false);
		} else {
			currLife = life;
		}

		rb.velocity = Vector2.zero;
		qte.end ();
	}

	public void Cutscene3(){
		isCutscene = true;
		snowHard.gameObject.SetActive (false);
		snowHardHard.gameObject.SetActive (true);
		StartCoroutine (cutscene3 ());
	}

	IEnumerator cutscene3(){
		PunhetarBotao qte = FindObjectOfType<PunhetarBotao> ();
		qte.StartQTE ();

		bool check = false;
		while (!qte.Ended() && currLife > 0) {
			isSnowing = false;

			currLife -= life / 1500;

			Color color = damageEffect.color;
			color.a = 1 - currLife / life;
			damageEffect.color = color;

			if (currLife <= 0) {
				check = true;
				Color _color = damageEffect.color;
				_color.a = 0;
				damageEffect.color = _color;
				gameManager.Restart (lastCheckpoint.x);
				currLife = life;
				enemies.RemoveRange (0, enemies.Count);
				anim.speed = 1f;
				canJump = true;
				EndCutscene ();
				canAttack = true;
				wrapStarted = false;
				qte.check = false;
				break;
			}

			yield return null;
		}

		if (!check) {
			anim.SetBool ("isMoving", false);
			StartCoroutine (restore ());
			flashback.Next ();
		}

		rb.velocity = Vector2.zero;
	}

	public void Cutscene1(){
		StartCoroutine (cutscene1 ());
	}

	IEnumerator cutscene1(){
		damageEffect.DOKill ();
		isCutscene = true;
		Vector2 tmp = rb.velocity;
		tmp.x = 0;
		rb.velocity = tmp;
		anim.SetBool ("isMoving", true);
		cutscene1Pos.y = transform.position.y;
		yield return transform.DOMoveX (cutscene1Pos.x, (transform.position - cutscene1Pos).magnitude / (velocityStates [state] * .7f)).SetEase (Ease.Linear).WaitForCompletion();
		anim.SetBool ("isMoving", false);
		yield return new WaitForSeconds (1.5f);

		List<Enemy> _enemies = new List<Enemy>();

		_enemies.Add (Instantiate (gameManager.enemy, transform.position + front [0], Quaternion.identity).GetComponent<Enemy> ());
		_enemies.Add (Instantiate (gameManager.enemy, transform.position + behind [1], Quaternion.identity).GetComponent<Enemy> ());

		_enemies.Add (Instantiate (gameManager.enemy, transform.position + front [1], Quaternion.identity).GetComponent<Enemy> ());
		_enemies.Add (Instantiate (gameManager.enemy, transform.position + behind [0], Quaternion.identity).GetComponent<Enemy> ());

		yield return null;

		bool check = true;

		//_enemies [0].GetComponent<SpriteRenderer> ().flipX = true;
		//_enemies [3].GetComponent<SpriteRenderer> ().flipX = true;

		foreach (var x in _enemies) {
			x.anim.SetTrigger ("attack" + (check ? "" : "2"));
			if (!check) {
				x.GetComponentInChildren<SpriteRenderer> ().sortingOrder = -1;
			}
			check = !check;
		}
	}
}
