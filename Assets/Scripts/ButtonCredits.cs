﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonCredits : MonoBehaviour {

	public Button back;
	public AudioClip clip;

	void Start()
	{
		Button btnBack = back.GetComponent<Button>();
		btnBack.onClick.AddListener(LoadMenu);
	}
		
	void LoadMenu()
	{
		SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
	}

	public void Load(){
		StartCoroutine (load());

	}

	public void adsa(){
		Application.Quit ();
	}

	IEnumerator load(){
		GetComponent<AudioSource>().clip = clip;
		GetComponent<AudioSource>().loop = false;
		GetComponent<AudioSource>().Play(); 
		yield return new WaitForSeconds (.3f);
		SceneManager.LoadScene (1);

	}
}
