﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuicktTimeEvent : MonoBehaviour {
	protected PlayerController player;

	private GameManager gameManager;

	void Start(){
		player = FindObjectOfType<PlayerController> ();
		gameManager = FindObjectOfType<GameManager> ();
	}

	// Quantidade de vezes em que o jogador poderá falhar antes de morrer e voltar ao último checkpoint

	protected void ResolveEvent(bool result){
		// TODO: Retornar uma mensagem para algum controlador, seja ele o do jogador ou do jogo com o resultado do QTE.
		if (!result) {
			gameManager.Restart (player.lastCheckpoint.x);
		}
	}

	virtual public void StartQTE (){
		// Imagina que é um método abstrato
	}

	virtual public void Restart (){

	}
}
