﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public float infSpeedLimit, supSpeedLimit;
	public float range;

	private int life = 1;
	private float speed;
	private PlayerController player;
	private Rigidbody2D rb;
	public SpriteRenderer sr;
	[HideInInspector]
	public Animator anim;
	private bool isAttacking;
	private GameManager gameManager;
	public GameObject deathEffect;
	public AudioClip deathClip;
	private AudioSource audioSource;
	private bool active = true;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		speed = Random.Range (infSpeedLimit, supSpeedLimit);
		player = FindObjectOfType<PlayerController> ();
		rb = GetComponent<Rigidbody2D> ();
		sr = GetComponentInChildren<SpriteRenderer> ();
		anim = GetComponent<Animator> ();
		gameManager = FindObjectOfType<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (active) {
			if (life <= 0) {
				Instantiate (deathEffect, transform.position, Quaternion.identity);
				sr.enabled = false;
				GetComponent<Collider2D> ().enabled = false;
				active = false;
				audioSource.clip = deathClip;
				audioSource.Play ();
				StartCoroutine (destroyAfter ());
			}
			if (!isAttacking) {
				float dir = player.transform.position.x - transform.position.x;
				Vector2 tmp = rb.velocity;
				tmp.x = Mathf.Sign (dir) * speed;

				Debug.DrawRay (transform.position, Vector2.right * Mathf.Sign (dir) * range, Color.red);
				RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.right * dir, range, 1 << LayerMask.NameToLayer ("Player"));

				if (hit) {
					isAttacking = true;
					tmp.x = 0;
					StartCoroutine (attack ());
				}
				rb.velocity = tmp;
			}
			sr.flipX = Mathf.Sign (player.transform.position.x - transform.position.x) == -1;
		}
	}

	IEnumerator destroyAfter(){
		yield return new WaitForSeconds (1f);
		Destroy (gameObject);
	}

	IEnumerator attack(){
		player.Wrap (this);
		if (!player.isCutscene) {
			anim.SetTrigger ("attack");
		}
		GetComponent<Collider2D> ().enabled = false;
		float dir = player.transform.position.x - transform.position.x;
		int num = Mathf.Sign (dir) < 0 ? 0 : 1;

		Vector3 pos = player.front [num];
		pos.x += player.transform.position.x;
		pos.y = gameManager.originalY;

		transform.position = pos;
		yield return null;
	}

	public void ReceiveDamage(int damage=1){
		life -= damage;
	}
}
