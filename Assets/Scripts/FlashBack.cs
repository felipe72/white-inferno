﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FlashBack : MonoBehaviour {

	public float appearTime;
	public float fadeTime;
	public Image img;
	public Sprite[] sprites;
	public Image blackOverlay;
	public GameObject flashback;

	private int currIndex = 0;
	private PlayerController player;
	private bool started = false;

	private AudioSource audioSource;

	public AudioClip[] clips;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Next(){
		if (!started) {
			started = true;
			StartCoroutine (next ());
		}
	}

	IEnumerator next(){
		yield return new WaitForSeconds (appearTime);
		if (player.state != 1 && player.state != 4) {
			img.sprite = sprites [currIndex];
			audioSource.clip = clips [currIndex];
			audioSource.Play ();

			currIndex++;
			yield return img.DOFade (1f, fadeTime).WaitForCompletion ();
			while (!Input.anyKey) {
				yield return new WaitForEndOfFrame ();
			}
			yield return img.DOFade (0f, fadeTime).WaitForCompletion ();

			player.ChangeState ();
			started = false;
		} else {
			for (int i = 0; i < 2; i++) {
				audioSource.clip = clips [currIndex];
				img.sprite = sprites [currIndex];
				if (i == 0) {
					audioSource.Play ();
				}
				currIndex++;

				yield return img.DOFade (1f, fadeTime).WaitForCompletion ();
				while (!Input.anyKey) {
					yield return new WaitForEndOfFrame ();
				}
				yield return img.DOFade (0f, fadeTime).WaitForCompletion ();
			}
			player.ChangeState ();
			started = false;
		}
	}
}
