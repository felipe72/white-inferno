﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraController : MonoBehaviour {
	private PlayerController player;
	[HideInInspector]
	public bool canFollow = true;

	public Rigidbody2D[] layer;

	private Rigidbody2D playerRb;

	public Vector3 lastPos;

	private Vector3 originalPosition;
	private Vector3[] originalLayerPosition;
	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		lastPos = player.transform.position;
		playerRb = player.GetComponent<Rigidbody2D> ();
		originalPosition = transform.position;
		originalLayerPosition = new Vector3[3];

		for (int i = 0; i < 3; i++) {
			originalLayerPosition [i] = layer [i].transform.position;
		}
	}

	// Update is called once per frame
	void Update () {
		if (canFollow) {
			tryToFollow ();
		}

		if (Input.GetKeyDown (KeyCode.K)) {
			Vector3 tmp = transform.position;
			tmp.x = player.transform.position.x;
			player.canWalk = false;
			transform.DOMove (tmp, 2f).SetEase (Ease.InOutCubic).OnComplete(() => {
				player.canWalk = true; 
				canFollow = true;
			});
		}
	}

	void tryToFollow(){
		// Melhorar isso daqui, colocar por exemplo um SmoothDamp top
		if (player.transform.position.x >= transform.position.x) {

			if (Mathf.Abs (lastPos.x - transform.position.x) < .01f) {
				// Não é pra dar DOKill aqui
			} else {
				transform.DOKill();
				transform.DOMoveX (player.transform.position.x, 1f).OnUpdate( () => {
					Vector3 pos = (originalPosition - transform.position);
					Vector3 poss = originalLayerPosition[2];
					poss.x -= pos.x/2;
					layer[2].transform.position = poss;

					pos = (originalPosition - transform.position);
					poss = originalLayerPosition[1];
					poss.x -= pos.x/4;
					layer[1].transform.position = poss;

					pos = (originalPosition - transform.position);
					poss = originalLayerPosition[0];
					poss.x += pos.x/4;
					layer[0].transform.position = poss;

				});
			}
			lastPos = player.transform.position;
		}
	}

	public void LockCamera(Vector3 position, float duration, int index){
		transform.DOMove (position, duration).SetEase (Ease.InOutCubic).OnComplete(() => {
			switch(index){
			case 1:
				player.Cutscene1();
				break;
			case 2:
				player.Cutscene2();
				break;
			case 3:
				player.Cutscene3();
				break;
			case 4:
				player.Cutscene4();
				break;
			case 6:
				player.EndCutscene ();
				player.crawl();
				break;
			}

		}).OnUpdate( () => {
			Vector3 pos = (originalPosition - transform.position);
			Vector3 poss = originalLayerPosition[2];
			poss.x -= pos.x/2;
			layer[2].transform.position = poss;

			pos = (originalPosition - transform.position);
			poss = originalLayerPosition[1];
			poss.x -= pos.x/4;
			layer[1].transform.position = poss;

			pos = (originalPosition - transform.position);
			poss = originalLayerPosition[0];
			poss.x += pos.x/4;
			layer[0].transform.position = poss;
		});
		canFollow = false;
	}
}