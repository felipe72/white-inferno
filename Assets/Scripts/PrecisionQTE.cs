﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PrecisionQTE : MonoBehaviour {

	public SpriteRenderer pointer;
	public SpriteRenderer limit;
	public float howMuch;
	public float duration;

	public float[] sizes;

	SpriteRenderer sr;
	int indexx = 0;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();

		pointer.transform.DOLocalMoveX (howMuch, duration).SetEase (Ease.Linear).SetLoops (-1, LoopType.Yoyo);
		//limit.transform.DOScaleX (sizes [index + 1], 10f).SetEase (Ease.Linear);
	}

	public void ScaleDown(){
		StartCoroutine (scaleDown ());
	}

	IEnumerator scaleDown(){
		indexx += 1;
		limit.DOKill ();
		yield return null;
		Vector3 tmp = limit.transform.localScale;
		print (indexx);
		tmp.x = sizes [indexx];
		limit.transform.localScale = tmp;
	}

	public bool GetResult(){
		return pointer.transform.position.x < limit.bounds.max.x && pointer.transform.position.x > limit.bounds.min.x;
	}
}
