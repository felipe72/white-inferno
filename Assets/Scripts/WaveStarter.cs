﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class myTriggerEvent : UnityEvent<Vector3, float, int>{}

public class WaveStarter : MonoBehaviour {
	public float duration;
	public Vector3 position;

	[SerializeField]
	public myTriggerEvent trigger;

	private bool triggered = false;
	private GameManager gameManager;
	private PlayerController player;

	public static int index = 4;

	void Start(){
		gameManager = FindObjectOfType<GameManager> ();
		player = FindObjectOfType<PlayerController> ();
	}

	public bool Trigger(){
		if (!triggered) {
			foreach (var x in FindObjectsOfType<Enemy>()) {
				x.ReceiveDamage (10);
			}
			gameManager.canSpawn = false;

			if (player.state == 4) {
				player.Cutscene5 ();
			} else {
				trigger.Invoke (transform.position + position, duration, player.state + 1);
			}
			triggered = true;
			return true;
		}

		return false;
	}

	public void Restart(){
		triggered = false;
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (transform.position + position, 1f);
	}
}
