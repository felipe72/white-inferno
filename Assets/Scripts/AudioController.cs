﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum AudioType { Level1, Level2, Level3, Level4, Level5, Level6 };

public class AudioController : MonoBehaviour {

	int index = 0;

	[System.Serializable]
	public class SerializableAudio{
		public AudioType type;
		public AudioClip clip;
	}

	public SerializableAudio[] audios;

	AudioSource audioSource;

	void Awake(){
		Screen.SetResolution (1280, 720, true);

		DontDestroyOnLoad (gameObject);
		audioSource = GetComponent<AudioSource> ();
		audioSource.Stop ();
	}

	private AudioClip findAudio(AudioType _audio){
		foreach(var audio in audios){
			if (audio.type == _audio) {
				return audio.clip;
			}
		}

		return null;
	}

	public void PlaySound(AudioType _audio, float duration=2f, bool loop = true){
		if (audioSource.isPlaying) {
			StartCoroutine (ChangeSound (_audio, duration, loop));
		} else {
			audioSource.clip = findAudio (_audio);
			audioSource.volume = 0;
			audioSource.Play ();
			audioSource.loop = loop;
			audioSource.DOFade (.22f, duration).SetUpdate (true);
		}
	}

	IEnumerator ChangeSound(AudioType _audio, float duration, bool loop = true){
		Stop (duration);
		yield return new WaitForSeconds (duration);
		audioSource.clip = findAudio (_audio);
		audioSource.volume = 0;
		audioSource.Play ();
		audioSource.loop = loop;
	
		audioSource.DOFade (.22f, duration).SetUpdate (true);
	}

	public void PlayNext(){
		PlaySound (audios [++index].type);
	}

	public void PlayOnce(AudioType _audio, float duration=1.5f){
		PlaySound (_audio, duration, false);
	}

	public void Stop(float duration=1.5f){
		StartCoroutine (stop (duration));
	}

	private IEnumerator stop(float duration){
		yield return audioSource.DOFade (0f, duration).WaitForCompletion();
	}
}
