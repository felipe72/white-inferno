﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour {
	public GameObject flashBack;

	public GameObject[] walls;
	public GameObject enemy;
	public QuicktTimeEvent[] Qtes;
	public WaveStarter[] waves;

	[HideInInspector]
	public PlayerController player;

	public bool canSpawn = true;

	[HideInInspector]
	public float originalY;

	// Use this for initialization
	void Start () {
		FindObjectOfType<AudioController> ().PlaySound (AudioType.Level1);

		Qtes = FindObjectsOfType<QuicktTimeEvent> ();
		waves = FindObjectsOfType<WaveStarter> ();
		player = FindObjectOfType<PlayerController> ();
		originalY = player.transform.position.y;

		SpawnEnemies ();
	}

	public void SpawnEnemies(){
		StartCoroutine (spawnEnemies());
	}

	public IEnumerator spawnEnemies(){
		while (canSpawn) {
			yield return new WaitForSeconds(Random.Range(0.5f, 2f));

			Vector3 pos = walls [Random.Range (0, 2)].transform.position - player.transform.position;

			pos.x += Mathf.Sign (pos.x) * 3f;
			pos += player.transform.position;
			pos.y = originalY;

			if (canSpawn) {
				Instantiate (enemy, pos, Quaternion.identity);
			}
		}

	}

	IEnumerator restart (){
		yield return player.blackOverlay.DOFade (1f, 1f).WaitForCompletion();
		player.Restart ();
		yield return player.blackOverlay.DOFade (0f, 1f).WaitForCompletion();
		if (player.state == 0 && !canSpawn) {
			canSpawn = true;
			SpawnEnemies ();
		}
	}

	public void Restart(float posX){
		StartCoroutine (restart());

		Camera.main.GetComponent<CameraController> ().canFollow = true;

		foreach (var x in FindObjectsOfType<Enemy>()) {
			Destroy (x.gameObject);
		}

		foreach (var x in Qtes) {
			if (x.transform.position.x > posX) {
				x.Restart ();
			}
		}

		foreach (var x in waves) {
			if (x.transform.position.x > posX) {
				x.Restart ();
			}
		}


	}

}
